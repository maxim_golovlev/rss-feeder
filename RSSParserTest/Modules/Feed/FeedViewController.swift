//
//  ViewController.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, BaseView {
    
    lazy var searchResult = SearchResultController.init(nibName: "SearchResultController", bundle: nil)
    
    lazy var searchController: UISearchController = {
        let search = UISearchController(searchResultsController: searchResult)
        search.searchResultsUpdater = searchResult
        search.delegate = self
        return search
    }()
    
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
            tableView.tableFooterView = UIView()
        }
    }
    
    var feeds = [RSSFeed]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        title = "RSS Feed"
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        feeds = RSSFeed.unarchive() ?? []
        feeds.sort(by: { $0.archiveDate! > $1.archiveDate! })
        tableView.reloadData()
    }
}

extension FeedViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
        cell.feed = feeds[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let feed = feeds[indexPath.row]
            
            do {
                try feed.delete()
            } catch {
                showAlert(message: ErrorType.deletingDataError.message)
                return
            }
            
            feeds.remove(at: indexPath.row)
            
            tableView.performBatchUpdates({ [weak self] in
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            }, completion: nil)
        }
    }
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        Router.openPostDetail(target: self, feed: feeds[indexPath.item], canSave: false)
    }
}

extension FeedViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        searchResult.feeds.removeAll()
        searchResult.tableView.reloadData()
    }
}


