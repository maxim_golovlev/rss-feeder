//
//  FeedDetailController.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class FeedDetailController: UIViewController {
    
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(UINib(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        }
    }
    
    var feed: RSSFeed?
    var canSave = false
    var posts = [Post]()
    
    lazy var postPresenter = PostPresenter(view: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = feed?.title

        postPresenter.fetchPosts(urlString: feed?.contentURL)
        
        if canSave {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveTapped))
        }
    }
    
    @objc func saveTapped(_ sender: Any) {
        
        if let saved = RSSFeed.unarchive(), saved.contains(where: { feed?.id == $0.id }) { return }
        
        do {
            feed?.archiveDate = Date()
            try feed?.archiveAdd()
        } catch {
            showAlert(message: (error as? ErrorType)?.message)
        }
    }
}

extension FeedDetailController: PostPresentee {
    func postsDidLoad(posts: [Post]) {
        self.posts = posts
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

extension FeedDetailController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        cell.post = posts[indexPath.row]
        return cell
    }
}

extension FeedDetailController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let post = posts[indexPath.row]
        
        guard let url = URL(string: post.link) else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}
