//
//  SearchResultController.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class SearchResultController: UIViewController {
    
    var feeds = [RSSFeed]()
    var timer: Timer?
    var currentTask: URLSessionDataTask?
    var lastQery: String = ""
    lazy var feedsPresenter = RssSearchPresenter(view: self)
    weak var searchController: UISearchController?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
            tableView.tableFooterView = UIView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}

extension SearchResultController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
        cell.feed = feeds[indexPath.row]
        return cell
    }
}

extension SearchResultController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        Router.openPostDetail(target: searchController?.presentingViewController, feed: feeds[indexPath.item], canSave: true)
    }
}


extension SearchResultController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.searchController = searchController
        
        guard let query = searchController.searchBar.text, !query.isEmpty, lastQery != query else { return }
        lastQery = query
        
        timer?.invalidate()
        currentTask?.cancel()
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { [weak self] (timer) in
            self?.currentTask = self?.feedsPresenter.searchFeed(query: query)
        })
    }
}

extension SearchResultController: RssSearchPresentee {
    func feedsDidLoad(feeds: [RSSFeed]) {
        self.feeds = feeds
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}
