//
//  String+Ext.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

extension String {
    
    func trimmWhiteSpace() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func setImageWidth(width: CGFloat) -> String {
        return "<head><style type=\"text/css\"> img{ max-height: 100%; max-width: \(width) !important; width: auto; height: auto;} </style> </head><body> \(self) </body>"
    }
    
    var html2AttributedString: NSMutableAttributedString? {
        do {
            return try NSMutableAttributedString(
                data: (self.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
        } catch {
            print(error)
            return nil
        }
    }
}
