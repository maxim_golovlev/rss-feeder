//
//  UIImageViewExtensions.swift
//  BackMyCash
//
//  Created by mac on 22.04.17.
//  Copyright © 2017 smscapital. All rights reserved.
//

import UIKit

class CashedImageView: UIImageView {
    
    static let imageCache = NSCache<NSString, AnyObject>()
    
    typealias ImageHandler = (UIImage) -> ()
    
    var urlForChecking: String?
    
    static var urlForCheckingStatic: String?
    
    func setImage(url: String?, handler: ImageHandler?) {
        
        guard let url = url else {
            return
        }
        
        let urlString = url
        
        urlForChecking = urlString
        CashedImageView.urlForCheckingStatic = urlString
        
        if let cachedImage = CashedImageView.imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            handler?(cachedImage)
            return
        }
        
        guard let URL = URL(string: urlString) else { return }
        
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: URL) else { return }
            
            DispatchQueue.main.async { [weak self] in
                guard let image = UIImage(data: data), (urlString == self?.urlForChecking || urlString == CashedImageView.urlForCheckingStatic) else { return }
                self?.image = image
                CashedImageView.imageCache.setObject(image, forKey: urlString as NSString)
                handler?(image)
            }
        }
    }
}


