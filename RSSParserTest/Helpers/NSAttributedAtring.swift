//
//  NSAttributedAtring.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setAlingment(_ alingment: NSTextAlignment) -> NSMutableAttributedString {
        
        let style = NSMutableParagraphStyle()
        style.alignment = alingment
        
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.paragraphStyle, value: style, range: range)
        return self
    }
    
    func setFont(_ font: UIFont?) -> NSMutableAttributedString {
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.font, value: font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize), range: range)
        return self
    }
    
    func setColor(_ color: UIColor) -> NSMutableAttributedString {
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.foregroundColor, value: color, range: range)
        return self
    }
    
    func setLink(text: String, url: URL?) -> NSMutableAttributedString {
        guard let range = string.range(of: text), let url = url else { return self }
        addAttributes([.link: url], range: NSRange( range, in: string))
        return self
    }
    
    func setParagraph(style: NSParagraphStyle) -> NSMutableAttributedString {
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.paragraphStyle, value: style, range: range)
        return self
    }
    
    func insertAttachment(image: UIImage) -> NSMutableAttributedString {
        
        let attachment = NSTextAttachment()
        attachment.image = image.withRenderingMode(.alwaysTemplate)
        let side = (attribute(.font, at: 0, effectiveRange: nil) as! UIFont).lineHeight - 2
        attachment.bounds = CGRect(x: 0, y: -2, width: side, height: side)
        
        let attachmentString = NSMutableAttributedString(attachment: attachment)
        attachmentString.append(NSAttributedString(string: " "))
        attachmentString.append(self)
        
        return attachmentString
    }
}

