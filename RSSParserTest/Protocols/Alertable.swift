import Foundation
import UIKit

protocol Alertable {
    func showAlert(message: String?)
    func showAlert(message: String?, handler: @escaping ((UIAlertAction) -> Void))
}

extension Alertable where Self: UIViewController {
    
    func showAlert(message: String?) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(close)

        DispatchQueue.main.async { [weak self] in
            self?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func showAlert(message: String?, handler: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: handler)
        alert.addAction(close)

        DispatchQueue.main.async { [weak self] in
            self?.present(alert, animated: true, completion: nil)
        }

    }
}
