//
//  BaseView.swift
//  Pet911
//
//  Created by User on 13.06.2019.
//  Copyright © 2019 pet. All rights reserved.
//

import Foundation

protocol BaseView: Alertable, Loadable { }
