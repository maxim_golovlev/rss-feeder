
import UIKit
import MBProgressHUD

protocol Loadable: class {
    func startLoading()
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    
    func startLoading() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.showAdded(to: self.view, animated: false)
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: false)
        }
    }
}

