//
//  Protocols.swift
//  Pet911
//
//  Created by User on 13.06.2019.
//  Copyright © 2019 pet. All rights reserved.
//

import Foundation

protocol Mappable: Archivable {
    init?(jsonString: String) throws
    init?(data: Data?) throws
    init?(json: [String: Any]?) throws
    
    var toJSON: [String: Any]? { get }
}

protocol Archivable: Codable {
    var id: String? { get set }
    static var fileUrl: String { get }
    func archiveAdd() throws
    func archive() throws
    static func delete() throws
    static func unarchive() -> [Self]?
}

extension Mappable {
    
    var toJSON: [String: Any]? {
        
        do {
            let data = try JSONEncoder().encode(self)
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            return json
            
        } catch let error {
            print("mapping from jsonString error", error.localizedDescription)
            return nil
        }
    }
    
    init?(jsonString: String) throws {
        guard let data = jsonString.data(using: .utf8) else {
            return nil
        }
        
        do {
            self = try JSONDecoder().decode(Self.self, from: data)
            
        } catch let error {
            print("mapping from jsonString error", error.localizedDescription)
            throw ErrorType.jsonConversionFailure
        }
    }
    
    init?(data: Data?) throws {
        
        guard let data  = data else { return nil }
        
        do {
            let item = try JSONDecoder().decode(Self.self, from: data)
            self = item
            
        } catch {
            print("mapping from data error", error.localizedDescription)
            throw ErrorType.jsonConversionFailure
        }
        
    }
    
    init?(json: [String: Any]?) throws {
        
        guard let json  = json else { return nil }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            
            let item = try JSONDecoder().decode(Self.self, from: data)
            self = item
            
        } catch {
            print("mapping from json error", error.localizedDescription)
            throw ErrorType.jsonConversionFailure
        }
    }
}

extension Array where Element: Archivable {
    
    func archive() throws {
        
        let data = try JSONEncoder().encode(self)
        let status = NSKeyedArchiver.archiveRootObject(data, toFile: Element.self.fileUrl)
        
        if !status {
            print("archiving error")
            throw ErrorType.archiveError
        }
    }
}

extension Archivable {
    
    static var fileUrl: String {
        return FileManager().urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(Self.self)").path
    }
    
    func archiveAdd() throws {
        
        var oldItems = Self.self.unarchive() ?? []
        oldItems.append(self)
        
        let data = try JSONEncoder().encode(oldItems)
        let status = NSKeyedArchiver.archiveRootObject(data, toFile: Self.fileUrl)
        
        if !status {
            print("archiving error")
            throw ErrorType.archiveError
        }
    }
    
    func archive() throws {
        
        let data = try JSONEncoder().encode([self])
        let status = NSKeyedArchiver.archiveRootObject(data, toFile: Self.fileUrl)
        
        if !status {
            print("archiving error")
            throw ErrorType.archiveError
        }
    }
    
    static func unarchive() -> [Self]? {
        
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: fileUrl) as? Data else {
            //print("unarchiveObject error: cannot get data from file")
            //throw ErrorType.unarchiveError
            return nil
        }
        
        let item = try? JSONDecoder().decode([Self].self, from: data)
        return item
    }
    
    func delete() throws {
        
        var oldItems = Self.self.unarchive() ?? []
        oldItems.append(self)
        
        oldItems.removeAll(where: { $0.id == self.id })
        
        let data = try JSONEncoder().encode(oldItems)
        let status = NSKeyedArchiver.archiveRootObject(data, toFile: Self.fileUrl)
        
        if !status {
            print("archiving error")
            throw ErrorType.archiveError
        }
    }
    
    
    static func delete() throws {
        
        do {
            try FileManager().removeItem(atPath: Self.fileUrl)
        }
        catch let error as NSError {
            print("deletingArchiveError", error)
            throw ErrorType.deletingDataError
        }
    }
}

