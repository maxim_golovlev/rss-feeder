//
//  Post.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

struct Post: Mappable {
    var id: String?
    var title: String
    var link: String
    var pubDate: String
    var description: String
}
