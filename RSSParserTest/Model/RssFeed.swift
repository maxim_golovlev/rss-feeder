//
//  RssFeed.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

struct RSSFeed: Mappable {
    var id: String?
    var title: String?
    var iconUrl: String?
    var description: String?
    var feedId: String?
    var archiveDate: Date?
    
    var contentURL: String? {
        return feedId?.replacingOccurrences(of: "feed/", with: "")
    }
    
    private enum CodingKeys : String, CodingKey {
        case id, title, iconUrl, description, feedId, archiveDate
    }
}
