//
//  RssSearchPresenter.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

protocol RssSearchPresentee: BaseView {
    func feedsDidLoad(feeds: [RSSFeed])
}

protocol RssSearchPresentable: class {
    var view: RssSearchPresentee? { get set }
    
    init(view: RssSearchPresentee)
    
    func searchFeed(query: String?) -> URLSessionDataTask?
}

extension RssSearchPresentable {
    
    func searchFeed(query: String?) -> URLSessionDataTask? {
        
        guard let query = query else { return nil }
        
        view?.startLoading()
        
        return NetworkManager.searchForFeed(query: query).fetch { [weak self] (response) in
            
            self?.view?.stopLoading()
            
            switch response {
            case .success(let data):
                
                do {
                    
                    guard let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any],
                    let array = dict["results"] as? [[String: Any]] else {
                        self?.view?.showAlert(message: ErrorType.responseUnsuccessful.message)
                        return
                    }
                    
                    let feeds = try array.compactMap({ try RSSFeed(json: $0) })
                    self?.view?.feedsDidLoad(feeds: feeds)
                    
                } catch {
                    self?.view?.showAlert(message: error.localizedDescription)
                }
                
            case .failed(let error):
                self?.view?.showAlert(message: error.message)
            }
        }
    }
}


class RssSearchPresenter: RssSearchPresentable {
    weak var view: RssSearchPresentee?
    
    required init(view: RssSearchPresentee) {
        self.view = view
    }
}

