//
//  PostPresenter.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

protocol PostPresentee: BaseView {
    func postsDidLoad(posts: [Post])
}

protocol PostPresentable: class {
    var view: PostPresentee? { get set }
    var parser: RSSParser? { get set }
    
    init(view: PostPresentee)
    
    func fetchPosts(urlString: String?)
}

extension PostPresentable {

    func fetchPosts(urlString: String?) {
        
        guard let urlString = urlString else { return }
        
        view?.startLoading()
        
        _ = NetworkManager.loadXML(urlString: urlString).fetch { [weak self] (response) in
            
            self?.view?.stopLoading()
            
            switch response {
            case .success(let data):
                
                self?.parser = RSSParser(data: data)
                self?.parser?.parse(completion: { [weak self] (response) in
                    
                    switch response {
                    case .success(let posts):
                        self?.view?.postsDidLoad(posts: posts)
                    case .failed(let error):
                        self?.view?.showAlert(message: error?.message)
                    }
                })
                
            case .failed(let error):
                self?.view?.showAlert(message: error.message)
            }
        }
    }
}


class PostPresenter: PostPresentable {
    var parser: RSSParser?
    weak var view: PostPresentee?
    
    required init(view: PostPresentee) {
        self.view = view
    }
}
