//
//  Router.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class Router {
    
    static func openPostDetail(target: UIViewController?, feed: RSSFeed?, canSave: Bool) {
        let vc = FeedDetailController.initWith(storyboradName: "Main", name: "FeedDetailController") as! FeedDetailController
        vc.feed = feed
        vc.canSave = canSave
        push(vc: vc, target: target)
    }
    
    static func push(vc: UIViewController, target: UIViewController?) {
        DispatchQueue.main.async {
            target?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
