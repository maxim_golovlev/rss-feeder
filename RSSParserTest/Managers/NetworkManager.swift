//
//  NetworkManager.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

enum NetworkManager {
    case loadXML(urlString: String)
    case searchForFeed(query: String)
}

enum ErrorType: Error {
    case requestFailed
    case responseUnsuccessful
    case jsonConversionFailure
    case invalidData
    case xmlParseError
    case archiveError
    case deletingDataError
    case custom(message: String)
    
    var message: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonConversionFailure: return "Json Conversion Failure"
        case .invalidData: return "Invalid Data"
        case .xmlParseError: return "Xml Parse Error"
        case .archiveError: return "Archiving Data Error"
        case .deletingDataError: return "Deleting Data Error"
        case .custom(let string): return string
        }
    }
}

enum Method: String {
    case get = "GET"
    case post = "POST"
}

extension NetworkManager {
    
    enum Response {
        case success(data: Data)
        case failed(error: ErrorType)
    }
    
    typealias NetworkCompletion = (Response) -> ()
    
    var method: Method {
        switch self {
        case .loadXML:
            return .get
        case .searchForFeed:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .loadXML:
            return [
                "Content-Type": "application/rss+xml",
                "Accept": "application/rss+xml"
                ]
        case .searchForFeed:
            return [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
        }
    }
    
    var session: URLSession {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }
    
    var targetURL: URL? {
        switch self {
        case .loadXML(let urlString):
            return URL(string: urlString)
        case .searchForFeed(let query):
            return URL(string: "https://cloud.feedly.com/v3/search/feeds/?query=\(query)")
        }
    }
    
    var params: [String: Any] {
        
        switch self {
        case .loadXML, .searchForFeed:
            return [:]
        }
    }
    
    var request: URLRequest? {
        
        guard let targetURL = targetURL else { return nil }
        
        var request = URLRequest(url: targetURL)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch {
            return nil
        }
        
        return request
    }
    
    func fetch(completion: @escaping NetworkCompletion) -> URLSessionDataTask? {
        
        guard let request = self.request else { return nil }
        
        Logger.request(request: targetURL?.absoluteString, action: "", params: params)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(Response.failed(error: ErrorType.requestFailed))
                return
            }
            
            guard httpResponse.statusCode == 200 else {
                completion(Response.failed(error: ErrorType.responseUnsuccessful))
                return
            }
            
            guard let data = data else {
                completion(Response.failed(error: ErrorType.invalidData))
                return
            }
            
            completion(Response.success(data: data))
            
        }
        
        task.resume()
        
        return task
    }
    
}
