//
//  RSSParser.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import Foundation

class RSSParser: NSObject {
    
    enum Response {
        case success(posts: [Post])
        case failed(error: ErrorType?)
    }
    
    private var parser: XMLParser?
    typealias ParseResult = (Response) -> ()
    private var completion: ParseResult?
    
    private var error: ErrorType?
    private var currentElement = ""
    private var tempPost: Post?
    private var posts = [Post]()
    
    init(data: Data) {
        super.init()
        
        parser = XMLParser(data: data)
        parser?.delegate = self
    }
    
    func parse(completion: ParseResult?) {
        
        self.completion = completion
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.parser?.parse()
            
            DispatchQueue.main.async { [weak self] in
                if let posts = self?.posts {
                    self?.completion?(.success(posts: posts) )
                } else {
                    self?.completion?(.failed(error: self?.error))
                }
            }
        }
    }
}

extension RSSParser: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        error = ErrorType.xmlParseError
        print("parse error: \(parseError)")
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        currentElement = elementName
        if elementName == "item" {
            tempPost = Post(id: UUID().uuidString, title: "", link: "", pubDate: "", description: "")
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if currentElement == "title" {
            tempPost?.title += string.trimmWhiteSpace()
        } else if currentElement == "link" {
            tempPost?.link += string.trimmWhiteSpace()
        } else if currentElement == "pubDate" {
            tempPost?.pubDate += string.trimmWhiteSpace()
        } else if currentElement == "description" {
            tempPost?.description += string.trimmWhiteSpace()
        }
        
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "item" {
            if let post = tempPost {
                posts.append(post)
            }
            tempPost = nil
        }
        
    }
}
