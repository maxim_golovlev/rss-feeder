//
//  FeedCell.swift
//  RSSParserTest
//
//  Created by  Macbook on 07/07/2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var iconView: CashedImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var feed: RSSFeed? {
        didSet {
            iconView.setImage(url: feed?.iconUrl, handler: nil)
            titleLabel.text = feed?.title
            subtitleLabel.text = feed?.description
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        iconView.image = nil
    }
}
