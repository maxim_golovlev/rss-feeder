//
//  PostCell.swift
//  RSSParserTest
//
//  Created by User on 28.06.2019.
//  Copyright © 2019 maxim.golovlev. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var post: Post? {
        didSet {
            titleLabel.text = post?.title
            dateLabel.text = post?.pubDate
            descriptionLabel.attributedText = post?.description.setImageWidth(width: bounds.width).html2AttributedString?.setFont(descriptionLabel.font)
        }
    }
}
